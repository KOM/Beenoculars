# Low Cost multi-user positionning system and caster

Hi! The aim of this project is to build low cost postionning system to track multiple assets using GPS and wireless 2,4Ghz communication system between the assets and the base station.
The base station embeds a web server permitting to serve webpages including map and assets location to subscibers (smartphone) using a wifi hotspot.
All the communications, from the subsciber to the tracked assets are free of charge, using 2,4Ghz band.

Keywords are :

> GPS, 2,4Ghz, Web Server.

Technologies used are :
>	RaspBerry Pi, Node JS, embeded C, FlySky-x6B module, STM32F031.

Potential applications :
- Regata tracking service for spectators for boat, rowing
- Construction machinery tracking service for road
- Players location system on sports field


# Contributors

All contributors are welcome.
This projects adresses 
- hardware (FlySky X6B module hacking, GPS module, batteries)
- Embeded software (embeded C for STM32F031) 
- Web software : NodeJS, JavaScript, Map
- DataBase : MongoDB


## Embeded device

The embeded device is based on a FlySky X6B module, a low cost GPS device (Antenna + GPS Chip) and a battery.
The GPS device is connected to the X6B module though the serial link.
The X6B module communicates with  the base using its 2,4Ghz radio link.
The Embeded device transmits its GPS location to the Base device using the radio link.
The first version of the project will use a direct communication from the embeded device to the base device.
A later version will use mesh network (like zigBee device) to enhance the range and decrease packet collision problem if more than 10 Embeded device are used simulteanously.

## Base device
The Base device is based on the same FlySky X6B module and is connected to a rasberry-pi using the USB Port.
The GPS locations are aquired from the radio link and transmitted to the local web server/caster

## Local Web Server/Caster

The local Web Server / Caster is a node.js application in the raspberry pi, permitting to acquire embeded devices locations.
The raspberry pi acts like a hotspot.
The subscriber connects to the hotspot withing their mobile phone or tablet using the Wifi connection.
The subscriber is redirected to a webPage displaying a map, and the location of the Embaded devices.

